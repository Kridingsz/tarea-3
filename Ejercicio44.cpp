#include <iostream>
#include <memory>


struct Camara 
{
 
 int cantidad_de_disparos;
 std::string marca;
int ano_de_fabricacion;

 /*std::shared_ptr<int> shared_ptr = std::shared_ptr<int>(new int);
 std::shared_ptr<int> shared_ptr = std::make_shared<int>(12);
 auto shared_ptr = std::make_shared<int>(12);

*/

Camara(int cantidad_de_disparos, std::string marca, int ano_de_fabricacion)
:cantidad_de_disparos(cantidad_de_disparos),marca(marca),ano_de_fabricacion(ano_de_fabricacion){}


};

void verificarCantidadDisparos(std::shared_ptr<Camara>& camaraPtr, int limite) {
    if (camaraPtr->cantidad_de_disparos >= limite) {
        camaraPtr.reset();
    }
}







int main() {

    int limite = 100;
    std::shared_ptr<Camara> camaraPtr = std::make_shared<Camara>(0, "Nikon", 2020);
    std::weak_ptr<Camara> weakPtr = camaraPtr;
    while (!weakPtr.expired() && camaraPtr->cantidad_de_disparos < limite) {
        camaraPtr->cantidad_de_disparos++;
        verificarCantidadDisparos(camaraPtr, limite);
    }
    if (weakPtr.expired()) {
        std::cout << "La cámara ha sido destruida\n";
    }
    return 0;



}